# Degree53 demo


## Requirements

Create a single page web application that utilises GitHub APIs to allow a user to search for a repository by name.  

The application should also offer a detail view for viewing more information about a repository. This detail view should include the repository’s readme, if available, and the number of open issues, number of forks, etc. 

## Getting started

Created with create-react-app.
So `yarn start` to run the application and `yarn test` to run the tests.

