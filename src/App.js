import React from 'react';
import axios from 'axios';

import PropTypes from 'prop-types';
import { compose } from 'recompose';

import { BehaviorSubject, combineLatest, timer } from 'rxjs';
import { flatMap, map, debounce, filter } from 'rxjs/operators';

import withStyles from '@material-ui/core/styles/withStyles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import DialogPreview from './components/DialogPreview';

import SearchInput from './components/SearchInput';
import Results from './components/Results';
import LinearProgress from '@material-ui/core/LinearProgress';

import config from './config/apis';

const styles = theme => ({
    appBar: {
        position: 'relative'
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 2,
        marginRight: theme.spacing.unit * 2,
        [theme.breakpoints.up(600 + theme.spacing.unit * 2 * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto'
        }
    }
});

const defaultSearchQuery = '';
const query$ = new BehaviorSubject(defaultSearchQuery);

const queryForFetch$ = query$.pipe(
    debounce(() => timer(500)),
    filter(query => query !== '')
);

const fetch$ = combineLatest(queryForFetch$).pipe(
    flatMap(([query]) => axios(config.searchUrl(query))),
    map(result => {
        return result.data.items;
    })
);

class AppBase extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            repos: this.props.repos,
            query: '',
            isLoading: false,
            observable: combineLatest(query$, fetch$, (query, repos) => ({
                query,
                repos,
                isLoading: false
            }))
        };
    }
    componentDidMount() {
        this.subscription = this.state.observable.subscribe(newState =>
            this.setState({ ...newState })
        );
    }

    componentWillUnmount() {
        this.subscription.unsubscribe();
    }

    handleClickOpen = (owner, name) => {
        axios(config.readMeUrl(owner, name)).then(r =>
            this.setState({ open: true, text: atob(r.data.content) })
        );
    };
    handleClose = () => {
        this.setState({ open: false });
    };
    onChangeHandler = value => {
        this.setState({ isLoading: true }, () => {
            query$.next(value);
        });
    };
    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <CssBaseline />
                <AppBar
                    position="absolute"
                    color="default"
                    className={classes.appBar}
                >
                    <Toolbar>
                        <Typography variant="h6" color="inherit" noWrap>
                            <SearchInput
                                name="searchRepos"
                                onChangeHandler={this.onChangeHandler}
                            />
                        </Typography>
                    </Toolbar>
                    {this.state.isLoading && <LinearProgress />}
                </AppBar>
                <main className={classes.layout}>
                    {!this.state.isLoading && (
                        <Results
                            repos={this.state.repos}
                            handleClickOpen={this.handleClickOpen}
                        />
                    )}
                </main>
                <DialogPreview
                    open={this.state.open}
                    onHandleClose={this.handleClose}
                    text={this.state.text}
                />
            </React.Fragment>
        );
    }
}

AppBase.propTypes = {
    classes: PropTypes.object.isRequired,
    repos: PropTypes.array
};

AppBase.defaultProps = {
    repos: []
};
const App = compose(withStyles(styles))(AppBase);

export default App;
