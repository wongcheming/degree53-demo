import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Markdown from 'react-markdown';

const DialogPreview = ({ open, onHandleClose, text }) => (
    <Dialog open={open} sroll="body" aria-labelledby="scroll-dialog-title">
        <DialogTitle id="scroll-dialog-title">ReadMe</DialogTitle>
        <DialogContent>
            <DialogContentText>
                <Markdown source={text} />
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={e => onHandleClose()} color="primary">
                Close
            </Button>
        </DialogActions>
    </Dialog>
);

export default DialogPreview;
