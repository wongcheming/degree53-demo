import React from 'react';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

export default ({ repos, handleClickOpen }) => (
    <List>
        {repos.map(repo => (
            <ListItem key={`repoName${repo.id}`} button>
                <ListItemText
                    onClick={e => handleClickOpen(repo.owner.login, repo.name)}
                    primary={repo.full_name ? repo.full_name : ''}
                    secondary={`forks: ${repo.forks_count}, open issues:${
                        repo.open_issues_count
                    }`}
                />
            </ListItem>
        ))}
    </List>
);
