import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import Results from './Results';

const mockRepos = [
    {
        full_name: 'test',
        forks_count: 1,
        open_issues_count: 1,
        owner: { login: 'owner1' },
        name: 'test'
    },
    {
        full_name: 'test',
        forks_count: 1,
        open_issues_count: 1,
        owner: { login: 'owner1' },
        name: 'test'
    },
    {
        full_name: 'test',
        forks_count: 1,
        open_issues_count: 1,
        owner: { login: 'owner1' },
        name: 'test'
    }
];

it('renders list of items', () => {
    const mockOpenDialog = jest.fn();

    const wrapper = shallow(
        <Results repos={mockRepos} handleClickOpen={mockOpenDialog} />
    );

    expect(wrapper.find(ListItem).length).toBe(3);
});

it('clicking on an item shuold open dialog', () => {
    const mockOpenDialog = jest.fn();

    const wrapper = shallow(
        <Results repos={mockRepos} handleClickOpen={mockOpenDialog} />
    );

    const listItem = wrapper.find(ListItemText).at(0);
    listItem.simulate('click');

    expect(mockOpenDialog).toBeCalled();
});
