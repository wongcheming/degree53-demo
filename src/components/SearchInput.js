import React from 'react';
import TextField from '@material-ui/core/TextField';

export default ({ onChangeHandler, onHandleSearch, name }) => (
    <TextField
        id="searchRepo"
        name={name}
        label="Search Github"
        fullWidth
        autoComplete="repoName"
        onChange={event => onChangeHandler(event.target.value)}
    />
);
