import React from 'react';
import { mount } from 'enzyme';
import SearchInput from './SearchInput';

it('should render input text and call function when text entered', () => {
    const mockChangeHandler = jest.fn();
    const wrapper = mount(<SearchInput onChangeHandler={mockChangeHandler} />);
    const searchInputBox = wrapper.find('input');
    searchInputBox.simulate('change', { target: { value: 'abc' } });
    expect(mockChangeHandler).toBeCalledWith('abc');
});
