export default {
    searchUrl: query => `https://api.github.com/search/repositories?q=${query}`,
    readMeUrl: (owner, name) =>`https://api.github.com/repos/${owner}/${name}/readme`
} 
